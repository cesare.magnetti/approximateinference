# Monte-Carlo Markov Chains & Variational Inference for Approximate Inference on Logistic Regression and Gaussian Process

This project was coursework for the Probabilistic Inference module from my MSc at Imperial. It focuses on intractable posterior approximation using MCMC and Variational Inference on two intractable case studies: 
1. Bayesian Logistic Regression posterior and predictive distribution;
2. Gaussian Process predictive distribution with intractable marginalisation of hyper-params

See following visualisations of the results:

<p>
<img align="left" width="49%" src="readme_images/logistic_MCMC.gif" alt="logistic regression posterior using MCMC"
title="logistic regression posterior using MCMC">

<img align="right" width="49%" src="readme_images/logistic_VI.gif" alt="logistic regression posterior using variational inference"
title="logistic regression posterior using variational inference" >
</p>

<br>

<p>
<img align="left" width="49%" src="readme_images/gp_MCMC.gif" alt="gaussian process posterior using MCMC"
title="gaussian process posterior using MCMC">

<img align="right" width="49%" src="readme_images/gp_VI.gif" alt="gaussian process posterior using variational inference"
title="gaussian process posterior using variational inference">
</p><br><br>

## Requirements
To install the requirements, use the following commands (with `python3>=3.6` enabled by default):
```bash
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
```

## Usage a visualisation

To visualize the results you can use the following snippets.


```bash
python -m distribution_prediction.metropolis_hastings.metropolis_hastings_logistic
python -m distribution_prediction.blackbox_vi.blackbox_vi_logistic

python -m distribution_prediction.metropolis_hastings.metropolis_hastings_gp
python -m distribution_prediction.blackbox_vi.blackbox_vi_gp
```

## Contributors
@cesare.magnetti

